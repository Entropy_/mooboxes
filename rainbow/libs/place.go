package libs

import (
	"fmt"
	"strings"
)

func FrameTitle(inWord string) (string, string) {
	var cel string
	wor := ""
	word := ""
	words := ""
	if len(inWord) > 68 {
		return "", "DONE COMPOSTING"
	}
	if len(inWord) > 19 && len(inWord) > 46 {
		wor += inWord[:19]
		word += inWord[23:46]
		words += inWord[46:]
		for i := len(words); i <= 19; i++ {
			words += " "
		}
	}
	if len(inWord) > 19 && len(inWord) < 46 {
		wor += inWord[:19]
		word += inWord[23:]
		for i := len(word); i <= 19; i++ {
			word += " "
		}
		words = "                    "

	}
	if len(inWord) < 19 {
		wor = "                    "
		word += " "
		word += inWord
		for i := len(word); i <= 19; i++ {
			word += " "
		}
		words = "                    "
	}
	cel += fmt.Sprint("\033[1;28H\033[48;2;10;255;20m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[2;28H\033[48;2;10;255;20m \033[48;2;10;10;20m", word, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[3;28H\033[48;2;10;255;20m \033[48;2;10;10;20m", words, "\033[48;2;10;255;20m \033[0m")
	fmt.Printf(cel)
	return fmt.Sprint(wor, word, words), cel
	//	fmt.Println(cel)
}

func AssembleComposeCel(inWord string) (string, string) {
	var cel string
	wor := ""
	word := ""
	words := ""
	if len(inWord) > 68 {
		return "", "DONE COMPOSTING"
	}
	if len(inWord) > 19 && len(inWord) > 46 {
		wor += inWord[:19]
		word += inWord[23:46]
		words += inWord[46:]
		for i := len(words); i <= 19; i++ {
			words += " "
		}
	}
	if len(inWord) > 19 && len(inWord) < 46 {
		wor += inWord[:19]
		word += inWord[23:]
		for i := len(word); i <= 19; i++ {
			word += " "
		}
		words = "                    "

	}
	if len(inWord) < 19 {
		wor = "                    "
		word += " "
		word += inWord
		for i := len(word); i <= 19; i++ {
			word += " "
		}
		words = "                    "
	}
	cel += fmt.Sprint("\033[22;28H\033[48;2;10;255;20m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[23;28H\033[48;2;10;255;20m \033[48;2;10;10;20m", word, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[24;28H\033[48;2;10;255;20m \033[48;2;10;10;20m", words, "\033[48;2;10;255;20m \033[0m")
	fmt.Printf(cel)
	return fmt.Sprint(wor, word, words), cel
	//	fmt.Println(cel)
}

func AssembleBlinkCel(object ActorS, X string, Y string) string {
	var cel string
	name := ""
	if object.Type == "Person" {
		if len(object.Content) > 57 {
			return "Error, content too long."
		}
		cel = fmt.Sprint("\033[1;1H\033[0m")
		wor := ""
		word := ""
		words := ""
		if len(object.Content) >= 57 {
			wor = object.Content[:19]
			word = object.Content[19:38]
			words = object.Content[38:57]
		}
		if len(object.Content) < 19 {
			wor = "                    "
			word += " "
			word += object.Content
			for i := len(word); i <= 19; i++ {
				word += " "
			}
			words = "                    "
		}
		if len(object.PreferredUsername) < 19 {
			name += "@" + object.PreferredUsername
			for i := len(name); i <= 19; i++ {
				name += " "
			}
		}
		object.Position.X = strings.Split(X, " ")
		object.Position.Y = strings.Split(Y, " ")
		cel += fmt.Sprint("\033[", object.Position.Y[0], ";", object.Position.X[0], "H \033[48;2;10;255;200m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[1], ";", object.Position.X[1], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[2], ";", object.Position.X[2], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", word, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[3], ";", object.Position.X[3], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", words, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[4], ";", object.Position.X[4], "H \033[48;2;10;255;200m\033[38;2;10;10;20m", name, "\033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[5], ";", object.Position.X[5], "H\033[0m")
		cel += fmt.Sprint("\033[20;1H\033[48;2;100;100;100m             \033[48;2;100;100;100m                                             \033[0m")
		cel += fmt.Sprint("\033[21;1H\033[48;2;0;100;100m             \033[48;2;0;125;125m\033[38;2;150;150;150mScuttlebutt\033[48;2;0;100;100m               \033[48;2;0;125;125m\033[38;2;150;150;150mBligLog\033[48;2;0;100;100m            \033[0m")

	}

	if object.Summary != "" && object.Type != "Person" {
		if len(object.Summary) > 57 {
			return "Error, content too long."
		}
		cel = fmt.Sprint("\033[1;1H\033[0m")
		wor := ""
		word := ""
		words := ""
		if len(object.Summary) >= 57 {
			wor = object.Summary[:19]
			word = object.Summary[19:38]
			words = object.Summary[38:57]
		}
		if len(object.Summary) < 19 {
			wor = "                    "
			word += " "
			word += object.Summary
			for i := len(word); i <= 19; i++ {
				word += " "
			}
			words = "                    "
		}
		if len(object.PreferredUsername) < 19 {
			name += "@" + object.PreferredUsername
			for i := len(name); i <= 19; i++ {
				name += " "
			}
		}
		object.Position.X = strings.Split(X, " ")
		object.Position.Y = strings.Split(Y, " ")
		cel += fmt.Sprint("\033[", object.Position.Y[0], ";", object.Position.X[0], "H \033[48;2;10;255;200m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[1], ";", object.Position.X[1], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[2], ";", object.Position.X[2], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", word, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[3], ";", object.Position.X[3], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", words, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[4], ";", object.Position.X[4], "H \033[48;2;10;255;200m\033[38;2;10;10;20m", name, "\033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[5], ";", object.Position.X[5], "H\033[0m")
		cel += fmt.Sprint("\033[20;1H\033[48;2;100;100;100m             \033[48;2;100;100;100m                                             \033[0m")
		cel += fmt.Sprint("\033[21;1H\033[48;2;0;100;100m             \033[48;2;0;125;125m\033[38;2;150;150;150mScuttlebutt\033[48;2;0;100;100m               \033[48;2;0;125;125m\033[38;2;150;150;150mBligLog\033[48;2;0;100;100m            \033[0m")

	}
	opened := false

	if opened {
		if len(object.Source["content"].(string)) > 57 {
			return "Error, content too long."
		}
		cel = fmt.Sprint("\033[1;1H\033[0m")
		wor := ""
		word := ""
		words := ""
		if len(object.Source["content"].(string)) >= 57 {
			wor = object.Source["content"].(string)[:19]
			word = object.Source["content"].(string)[19:38]
			words = object.Source["content"].(string)[38:57]
		}
		if len(object.Source["content"].(string)) < 19 {
			wor = "                    "
			word += " "
			word += object.Source["content"].(string)
			for i := len(word); i <= 19; i++ {
				word += " "
			}
			words = "                    "
		}
		object.Position.X = strings.Split(X, "")
		object.Position.Y = strings.Split(Y, "")
		cel += fmt.Sprint("\033[", object.Position.Y[0], ";", object.Position.X[0], "H \033[48;2;10;255;200m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[1], ";", object.Position.X[1], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[2], ";", object.Position.X[2], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", word, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[3], ";", object.Position.X[3], "H\033[48;2;10;255;200m \033[48;2;10;10;20m", words, "\033[48;2;10;255;200m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[4], ";", object.Position.X[4], "H \033[48;2;10;255;200m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[5], ";", object.Position.X[5], "H\033[0m")
		cel += fmt.Sprint("\033[20;1H\033[48;2;100;100;100m             \033[48;2;100;100;100m                                             \033[0m")
		cel += fmt.Sprint("\033[21;1H\033[48;2;0;100;100m             \033[48;2;0;125;125m\033[38;2;150;150;150mScuttlebutt\033[48;2;0;100;100m               \033[48;2;0;125;125m\033[38;2;150;150;150mBligLog\033[48;2;0;100;100m            \033[0m")

	}

	return cel
	//	fmt.Println(cel)
}

func AssembleCel(object ActorS, X string, Y string) string {
	var cel string
	name := ""
	if object.Type == "Person" {
		if len(object.Content) > 57 {
			return "Error, content too long."
		}
		cel = fmt.Sprint("\033[1;1H\033[0m")
		wor := ""
		word := ""
		words := ""
		if len(object.Content) >= 57 {
			wor = object.Content[:19]
			word = object.Content[19:38]
			words = object.Content[38:57]
		}
		if len(object.Content) < 19 {
			wor = "                    "
			word += " "
			word += object.Content
			for i := len(word); i <= 19; i++ {
				word += " "
			}
			words = "                    "
		}
		if len(object.PreferredUsername) < 19 {
			name += "@" + object.PreferredUsername
			for i := len(name); i <= 19; i++ {
				name += " "
			}
		}
		object.Position.X = strings.Split(X, " ")
		object.Position.Y = strings.Split(Y, " ")
		cel += fmt.Sprint("\033[", object.Position.Y[0], ";", object.Position.X[0], "H \033[48;2;10;255;20m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[1], ";", object.Position.X[1], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[2], ";", object.Position.X[2], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", word, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[3], ";", object.Position.X[3], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", words, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[4], ";", object.Position.X[4], "H \033[48;2;10;255;20m\033[38;2;10;10;20m", name, "\033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[5], ";", object.Position.X[5], "H\033[0m")
		cel += fmt.Sprint("\033[20;1H\033[48;2;100;100;100m             \033[48;2;100;100;100m                                             \033[0m")
		cel += fmt.Sprint("\033[21;1H\033[48;2;0;100;100m             \033[48;2;0;125;125m\033[38;2;150;150;150mScuttlebutt\033[48;2;0;100;100m               \033[48;2;0;125;125m\033[38;2;150;150;150mBligLog\033[48;2;0;100;100m            \033[0m")

	}

	if object.Summary != "" && object.Type != "Person" {
		if len(object.Summary) > 57 {
			return "Error, content too long."
		}
		cel = fmt.Sprint("\033[1;1H\033[0m")
		wor := ""
		word := ""
		words := ""
		if len(object.Summary) >= 57 {
			wor = object.Summary[:19]
			word = object.Summary[19:38]
			words = object.Summary[38:57]
		}
		if len(object.Summary) < 19 {
			wor = "                    "
			word += " "
			word += object.Summary
			for i := len(word); i <= 19; i++ {
				word += " "
			}
			words = "                    "
		}
		if len(object.PreferredUsername) < 19 {
			name += "@" + object.PreferredUsername
			for i := len(name); i <= 19; i++ {
				name += " "
			}
		}
		object.Position.X = strings.Split(X, " ")
		object.Position.Y = strings.Split(Y, " ")
		cel += fmt.Sprint("\033[", object.Position.Y[0], ";", object.Position.X[0], "H \033[48;2;10;255;20m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[1], ";", object.Position.X[1], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[2], ";", object.Position.X[2], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", word, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[3], ";", object.Position.X[3], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", words, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[4], ";", object.Position.X[4], "H \033[48;2;10;255;20m\033[38;2;10;10;20m", name, "\033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[5], ";", object.Position.X[5], "H\033[0m")
		cel += fmt.Sprint("\033[20;1H\033[48;2;100;100;100m             \033[48;2;100;100;100m                                             \033[0m")
		cel += fmt.Sprint("\033[21;1H\033[48;2;0;100;100m             \033[48;2;0;125;125m\033[38;2;150;150;150mScuttlebutt\033[48;2;0;100;100m               \033[48;2;0;125;125m\033[38;2;150;150;150mBligLog\033[48;2;0;100;100m            \033[0m")

	}
	opened := false

	if opened {
		if len(object.Source["content"].(string)) > 57 {
			return "Error, content too long."
		}
		cel = fmt.Sprint("\033[1;1H\033[0m")
		wor := ""
		word := ""
		words := ""
		if len(object.Source["content"].(string)) >= 57 {
			wor = object.Source["content"].(string)[:19]
			word = object.Source["content"].(string)[19:38]
			words = object.Source["content"].(string)[38:57]
		}
		if len(object.Source["content"].(string)) < 19 {
			wor = "                    "
			word += " "
			word += object.Source["content"].(string)
			for i := len(word); i <= 19; i++ {
				word += " "
			}
			words = "                    "
		}
		object.Position.X = strings.Split(X, "")
		object.Position.Y = strings.Split(Y, "")
		cel += fmt.Sprint("\033[", object.Position.Y[0], ";", object.Position.X[0], "H \033[48;2;10;255;20m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[1], ";", object.Position.X[1], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[2], ";", object.Position.X[2], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", word, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[3], ";", object.Position.X[3], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", words, "\033[48;2;10;255;20m \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[4], ";", object.Position.X[4], "H \033[48;2;10;255;20m                    \033[0m")
		cel += fmt.Sprint("\033[", object.Position.Y[5], ";", object.Position.X[5], "H\033[0m")
		cel += fmt.Sprint("\033[20;1H\033[48;2;100;100;100m             \033[48;2;100;100;100m                                             \033[0m")
		cel += fmt.Sprint("\033[21;1H\033[48;2;0;100;100m             \033[48;2;0;125;125m\033[38;2;150;150;150mScuttlebutt\033[48;2;0;100;100m               \033[48;2;0;125;125m\033[38;2;150;150;150mBligLog\033[48;2;0;100;100m            \033[0m")

	}

	return cel
	//	fmt.Println(cel)
}

func main() {
	//var obj Object
	//AssembleCel(obj)
	//time.Sleep(3 * time.Second)
	//for i := 0; i < 64; i++ {
	//	fmt.Println(" ")
	//}
	//obj.content = "GREETINGS"
	//AssembleCel(obj)
	//time.Sleep(3 * time.Second)
	//for i := 0; i < 64; i++ {
	//	fmt.Println(" ")
	//}
	//obj.content = "I WENT TO THE STORE TODAY AND I HAD A PIECE OF TOAST AND THEN YOU THREW AN OCOTPUS AT MY WINDOW"
	//AssembleCel(obj)
	//time.Sleep(3 * time.Second)
	//for i := 0; i < 64; i++ {
	//	fmt.Println(" ")
	//}
	//	EncodeApp()
	//	EncodeGroup()
	//	EncodeOrg()
	//	EncodePerson()
	//	EncodeService()
}
