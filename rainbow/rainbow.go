package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"gitlab.com/owlo/Ogopogo-clients/rainbow/libs"

	"github.com/tidwall/sjson"
)

var hostname string
var username string
var password string

func validate(signature string, file []byte) {

	bufferedReader, err := os.Create("tests/outfile")
	defer os.Remove("tests/outfile")
	if err != nil {
		fmt.Println("Error creating outfile")
	}
	bufferedReader.Write(file)
	bufferedReader.Close()
	decrypter := exec.Command("/bin/sh", "-c", "echo crashandburn | gpg --no-tty --passphrase-fd 0 -o out.json outfile")
	decrypter.Dir = "tests/"
	decrypter.Run()

	//if err != nil {
	//	fmt.Println("Error running command")
	//	fmt.Println(err)
	//}
}

func validatePlayer(signature string, file []byte) {

	bufferedReader, err := os.Create("tests/outfile")
	defer os.Remove("tests/outfile")
	if err != nil {
		fmt.Println("Error creating outfile")
	}
	bufferedReader.Write(file)
	bufferedReader.Close()
	decrypter := exec.Command("/bin/sh", "-c", "echo crashandburn | gpg --no-tty --passphrase-fd 0 -o player.json outfile")
	decrypter.Dir = "tests/"
	decrypter.Run()

	//if err != nil {
	//	fmt.Println("Error running command")
	//	fmt.Println(err)
	//}
}

func ParseInput(inpoot string, stdin chan string, client *http.Client) string {

	switch inpoot {
	//AVATARCOMMAND
	case "c":
		reqUID, err := http.NewRequest("GET", "https://"+hostname+":8080/exampleMobile", strings.NewReader("NEWMOBILE"))
		reqUID.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
		mName := ""
		char := ""
		character := ""
		fmt.Printf("Enter name of mobile:")
		for char != "\n" {

			fmt.Printf(char)
			char = <-stdin
			character += char
		}
		mName = strings.Trim(character, "\n")
		reqUID.Header.Add("Mobile-Name", mName)
		resp, err := client.Do(reqUID)
		if err != nil {
			fmt.Println("error crafting UID")
		}
		UID, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(string(UID))
	case "t":
		reqUID, err := http.NewRequest("GET", "https://mastodon.social/#Outbox", strings.NewReader("BORKUIDPLS"))

		reqUID.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

		resp, err := client.Do(reqUID)
		if err != nil {
			fmt.Println("error crafting UID")
		}
		UID, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(string(UID))
	case ".":
		fmt.Println("\033[19;3HCOMPOSE CEL COMMAND GIVEN")
		reqUID, err := http.NewRequest("GET", "https://"+hostname+":8080/getuid", strings.NewReader("BORKUIDPLS"))

		reqUID.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

		resp, err := client.Do(reqUID)
		if err != nil {
			fmt.Println("error crafting UID")
		}
		UID := resp.Header.Get("UID")
		//fmt.Println(UID)
		resp.Body.Close()
		newCel := ""
		//newCelShown := ""
		fmt.Printf("\033[22;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[23;28H\033[48;2;10;255;20m \033[48;2;10;10;20m     Compose Title!    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[24;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")

		for len(newCel) < 68 {
			inp := <-stdin
			newCel += inp
			if inp == "\u007F" {
				if len(newCel) > 1 {
					newCel = newCel[:len(newCel)-2]
				}
			}
			libs.AssembleComposeCel(newCel)
			//fmt.Printf(newCelShown)
			if len(newCel) >= 68 {
				fmt.Printf("\033[19;3HABORT CEL COMMAND GIVEN")
				return "."
			}
			if inp == "\u000f" || inp == "\n" {
				for i := 0; i < 83; i++ {
					fmt.Println("")
				}
				//fmt.Printf("SET Title Bork COMMAND GIVEN")
				newCel = strings.Trim(newCel, "\u000f")
				newCel = strings.Trim(newCel, "\n")
				ok := PostCel(stdin, newCel, UID, client)
				if ok != false {
					fmt.Printf("\033[19;3HSEND CEL COMMAND COMPLETED")
				}
				for i := 0; len(newCel) < 68; i++ {
					newCel += " "
				}

			}
		}
	//	fmt.Printf(newCelShown)
	case "k":
		return inpoot
	case "-":
		for x := 0; x < 80; x++ {
			for y := 0; y < 24; y++ {
				trans := fmt.Sprint("\033[", y, ";", x, "H\033[48;2;100;100;100m \033[0m")
				fmt.Printf(trans)
				time.Sleep(200000 * time.Nanosecond)
			}
		}
		for i := 0; i < 80; i++ {
			fmt.Println("")
		}
		time.Sleep(500 * time.Millisecond)
		//fmt.Println("VIEW RIGHT COMMAND GIVEN")
	case "+":
		for x := 80; x > 0; x-- {
			for y := 24; y > 0; y-- {
				trans := fmt.Sprint("\033[", y, ";", x, "H\033[48;2;100;100;100m \033[0m")
				fmt.Printf(trans)
				time.Sleep(200000 * time.Nanosecond)
			}
		}
		for i := 0; i < 80; i++ {
			fmt.Println("")
		}
		time.Sleep(500 * time.Millisecond)
		//fmt.Println("VIEW LEFT COMMAND GIVEN")
	case "0":
		lpFile, err := os.Create("tests/lp")
		if err != nil {
			fmt.Println("Error creating lp file.")
		}
		lpFile.Write([]byte("0"))
		//fmt.Println("PAWS COMMAND GIVEN")
	default:

	}

	return inpoot
}

func HandleInput(out chan string, in chan bool) {
	//flour.ToastLogger("readStdin")
	//no buffering
	exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
	//no visible output
	exec.Command("stty", "-F", "/dev/tty", "-echo").Run()
	// restore the echoing state when exiting
	defer exec.Command("stty", "-F", "/dev/tty", "echo").Run()

	var b = make([]byte, 1)
	for {
		os.Stdin.Read(b)
		out <- string(b)

	}
}
func PlaceCel(index int, mockActor []libs.ActorS, X string, Y string, stdin chan string, client *http.Client) string {
	//Uncomment the next two lines to enable blinky cels
	blinkCel := libs.AssembleBlinkCel(mockActor[index], X, Y)
	fmt.Println(blinkCel)
	//
	//time.Sleep(500 * time.Millisecond)
	select {
	case inpoot := <-stdin:
		inp := ParseInput(inpoot, stdin, client)
		if inp == "k" {
			return "atac"
		}
		if inp == "2" {
			return "south"
		}
		if inp == "4" {
			return "west"
		}
		if inp == "6" {
			return "east"
		}
		if inp == "8" {
			return "north"
		}
		if inp == "1" {
			return "southwest"
		}
		if inp == "7" {
			return "northwest"
		}
		if inp == "3" {
			return "southeast"
		}
		if inp == "9" {
			return "northeast"
		}

		if inp == "pause" {
			inp = <-stdin
		}
	default:
		fmt.Printf("\033[19;3HNO COMMAND DETECTED")
	}
	return ""
}
func UIDMaker() string {
	hostname := "localhost"
	username := "username"
	//Inspired by 'Una (unascribed)'s bikeshed
	rand.Seed(int64(time.Now().Nanosecond()))
	adjectives := []string{"Accidental", "Allocated", "Asymptotic", "Background", "Binary",
		"Bit", "Blast", "Blocked", "Bronze", "Captured", "Classic",
		"Compact", "Compressed", "Concatenated", "Conventional",
		"Cryptographic", "Decimal", "Decompressed", "Deflated",
		"Defragmented", "Dirty", "Distinguished", "Dozenal", "Elegant",
		"Encrypted", "Ender", "Enhanced", "Escaped", "Euclidean",
		"Expanded", "Expansive", "Explosive", "Extended", "Extreme",
		"Floppy", "Foreground", "Fragmented", "Garbage", "Giga", "Gold",
		"Hard", "Helical", "Hexadecimal", "Higher", "Infinite", "Inflated",
		"Intentional", "Interlaced", "Kilo", "Legacy", "Lower", "Magical",
		"Mapped", "Mega", "Nonlinear", "Noodle", "Null", "Obvious", "Paged",
		"Parity", "Platinum", "Primary", "Progressive", "Prompt",
		"Protected", "Quick", "Real", "Recursive", "Replica", "Resident",
		"Retried", "Root", "Secure", "Silver", "SolidState", "Super",
		"Swap", "Switched", "Synergistic", "Tera", "Terminated", "Ternary",
		"Traditional", "Unlimited", "Unreal", "Upper", "Userspace",
		"Vector", "Virtual", "Web", "WoodGrain", "Written", "Zipped"}
	nouns := []string{"AGP", "Algorithm", "Apparatus", "Array", "Bot", "Bus", "Capacitor",
		"Card", "Chip", "Collection", "Command", "Connection", "Cookie",
		"DLC", "DMA", "Daemon", "Data", "Database", "Density", "Desktop",
		"Device", "Directory", "Disk", "Dongle", "Executable", "Expansion",
		"Folder", "Glue", "Gremlin", "IRQ", "ISA", "Instruction",
		"Interface", "Job", "Key", "List", "MBR", "Map", "Modem", "Monster",
		"Numeral", "PCI", "Paradigm", "Plant", "Port", "Process",
		"Protocol", "Registry", "Repository", "Rights", "Scanline", "Set",
		"Slot", "Smoke", "Sweeper", "TSR", "Table", "Task", "Thread",
		"Tracker", "USB", "Vector", "Window"}
	uniquefier := ""

	uniqe := ""
	for i := 0; i < 2; i++ {
		uniq := rand.Intn(15)
		if uniq >= 10 {
			switch uniq {
			case 10:
				uniqe = "A"
			case 11:
				uniqe = "B"
			case 12:
				uniqe = "C"
			case 13:
				uniqe = "D"
			case 14:
				uniqe = "E"
			case 15:
				uniqe = "F"
			}

			uniquefier += uniqe
		} else {
			uniquefier += fmt.Sprint(uniq)
		}
	}
	ind := rand.Intn(len(adjectives))
	indie := rand.Intn(len(adjectives))
	if indie == ind {
		indie = rand.Intn(len(adjectives))
	}
	thedog := rand.Intn(len(nouns))
	uniqueFied := fmt.Sprint(uniquefier, adjectives[ind], adjectives[indie], nouns[thedog])

	//fmt.Println(uniqueFied)

	UID := fmt.Sprint("/posts/", uniqueFied, hostname, username)
	return UID
}
func MarshalCel(path string, cel string, valueToChange string, UID string) string {
	actorRead, err := ioutil.ReadFile(path)
	newActorFile, err := os.Create("tests/scree.json")
	defer newActorFile.Close()

	values, err := sjson.Set(string(actorRead), valueToChange, cel)
	values, err = sjson.Set(values, "id", UID)
	if err != nil {
		fmt.Println("Error sjsoning")
	}
	fmt.Println("\033[38;2;255;0;0m" + values + "\033[0m")
	if err != nil {
		fmt.Println("Error writing json")
		fmt.Println(err)
	}
	//fmt.Println(string(value) + "JSON REPRESENATIATION")
	newActorFile.WriteString(values)

	return "tests/scree.json"
}
func PostCel(stdin chan string, cel string, UID string, client *http.Client) bool {

	//fmt.Scan(&input)
	file, err := os.Create("tests/seagull")
	defer file.Close()
	if err != nil {
		fmt.Println("error creating file")
	}
	writer := io.Writer(file)
	//	path := "tests/seagull.json"
	path := MarshalCel("tests/seagull.json", cel, "summary", UID)
	celTitle := cel
	cel = ""
	newChar := ""
	screen := ""
	for c := 1; c <= 24; c++ {

		for r := 1; r <= 80; r++ {
			if (r == 1 && c <= 3) || r == 80 && c <= 3 || c <= 3 || c == 24 {
				screen += fmt.Sprint("\033[48;2;100;100;100m \033[0m")
			} else {
				screen += fmt.Sprint(" ")
			}
		}
	}

	_, newCelShown := libs.FrameTitle(celTitle)
	wordCount := strconv.Itoa(len(strings.Split(cel, " ")))
	characterCount := strconv.Itoa(len(cel))
	fmt.Printf("\033[1;1H" + screen + "\033[0m")
	fmt.Printf(newCelShown)
	fmt.Printf("\033[1;1H\033[38;0;255;0m" + characterCount + "/5000\033[0m")
	fmt.Printf("\033[2;1H\033[38;0;255;0m" + wordCount + "\033[0m")
	fmt.Printf("\033[4;1H" + cel + "\033[0m")
	for {
		newChar = <-stdin
		if newChar == "\u000f" {
			break
		} else {
			if newChar == "\u007F" {
				if len(cel) > 1 {
					cel = cel[:len(cel)-1]
				}
				wordCount = strconv.Itoa(len(strings.Split(cel, " ")))
				characterCount = strconv.Itoa(len(cel))
				fmt.Printf("\033[1;1H" + screen + "\033[0m")
				fmt.Printf(newCelShown)
				fmt.Printf("\033[1;1H\033[38;0;255;0m" + characterCount + "/5000\033[0m")
				fmt.Printf("\033[2;1H\033[38;0;255;0m" + wordCount + "\033[0m")
				fmt.Printf("\033[4;1H" + cel + "\033[0m")
			} else {
				cel += newChar
				wordCount = strconv.Itoa(len(strings.Split(cel, " ")))
				characterCount = strconv.Itoa(len(cel))
				fmt.Printf("\033[1;1H" + screen + "\033[0m")
				fmt.Printf(newCelShown)
				fmt.Printf("\033[1;1H\033[38;0;255;0m" + characterCount + "/5000\033[0m")
				fmt.Printf("\033[2;1H\033[38;0;255;0m" + wordCount + "\033[0m")
				fmt.Printf("\033[4;1H" + cel + "\033[0m")
			}
		}
	}

	path = MarshalCel(path, cel, "content", string(UID))
	signedContent, err := ioutil.ReadFile(path)
	writer.Write(signedContent)
	//fmt.Println(string(signedContent))
	//fmt.Println("Signing complete.")
	//body := bufio.NewReader(file)
	//req, err := http.NewRequest("POST", "https://"+hostname+":8080/exampleposter", strings.NewReader(string(signedContent)))
	req, err := http.NewRequest("POST", "https://"+hostname+":8080/post", strings.NewReader(string(signedContent)))
	//TODO
	req.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"") 
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	_, err = client.Do(req)
	if err != nil {
		fmt.Println("Error sending request.")
		fmt.Println(err)
	}
	for i := 0; i < 82; i++ {
		fmt.Println("")
	}
	return true
}
func initActors(currentActors string) libs.ActorS {
	actorFile, err := os.Create("tests/tempfile")
	actorFile.Write([]byte(currentActors))
	defer actorFile.Close()
	if err != nil {
		fmt.Println("Error, cannot find current actor file.")
	}
	actorReader := []byte(currentActors)

	var mockup libs.ActorS
	container := make(map[string]interface{})
	err = json.Unmarshal(actorReader, &mockup)
	err = json.Unmarshal(actorReader, &container)
	if err != nil {
		fmt.Println("Error unmarshalling json.")
		fmt.Println(err)
	}
	k := make([]string, len(container))
	index := 0
	for i, _ := range container {
		k[index] = i

		if k[index] == "@context" {
			con := container["@context"]
			mockup.Context = con
		}
		index++
	}
	//fmt.Println(k)
	//fmt.Println(string(actorReader))
	//fmt.Println(mockup)
	return mockup
}

func getPC(client *http.Client) libs.ActorS {

	request, err := http.NewRequest("GET", "https://"+hostname+":8080/examplePC", nil)
	if err != nil {
		fmt.Println(err)
	}
	request.Header.Add("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
	request.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

	resp, err := client.Do(request)

	PC, err := ioutil.ReadAll(resp.Body)
	//fmt.Println(string(PC))
	objectFile, err := os.Create("tests/player.json")
	if err != nil {
		fmt.Println(err)
	}
	objectFile.Write(PC)
	objectFile.Close()
	//	validatePlayer("signature", PC)
	playerFile, err := os.Open("tests/player.json")
	playerSheet, err := ioutil.ReadAll(playerFile)
	os.Remove("tests/player.json")
	if err != nil {
		fmt.Println("Error reading playerfile")
	}
	player := initActors(string(playerSheet))
	return player
}

func GetCels(stdin chan string, lp string, client *http.Client) (bool, string) {
	//fmt.Scan(&input)
	request, err := http.NewRequest("GET", "https://"+hostname+":8080/examplepost", nil)
	if err != nil {
		fmt.Println(err)
	}
	request.Header.Add("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
	resp, err := client.Do(request)
	if err != nil {
		fmt.Println("error with request")
		fmt.Println(err)
	}
	borks := resp.Header.Get("NumBorks")
	//borks = strings.Trim(borks, "\"")
	//numBorks, err := strconv.Atoi(borks)
	//numBorkSeen, err := strconv.Atoi(lp)
	if err != nil {
		fmt.Println("Cannot convert borks!")
	}
	//fmt.Println("BORKS", borks, lp)
	if borks != lp {
		request, err = http.NewRequest("GET", "https://"+hostname+":8080/examplepost", nil)
		if err != nil {
			fmt.Println(err)
		}
		request.Header.Add("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
		request.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

		resp, err = client.Do(request)

		//defer resp.Body.Close()
		if err != nil {
			fmt.Println("Error sending request.")
			fmt.Println(err)
		}
		serverSignature := resp.Header.Get("Authorization")
		lastPost := resp.Header.Get("LastPost")
		fmt.Println(serverSignature)
		if err != nil {
			fmt.Println("Authentication error!")
		}

		object, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Println("Error reading reponse.")
			fmt.Println(err)
		}
		//beachBall := object
		//fmt.Println(beachBall)
		//validate(serverSignature, object)
		objectFile, err := os.Create("tests/out.json")
		if err != nil {
			fmt.Println(err)
		}
		objectFile.Write(object)
		objectFile.Close()

		if err != nil {
			fmt.Println("\033[;38;2;255;0;0mError\033[0m crafting request")
			fmt.Println(err)
		}
		noNew := false
		maxIndex := 0
		mockActor := UnmActors("tests/out.json")
		for i := 0; i < len(mockActor)-1; i++ {
			if i == len(mockActor)-2 {
				if mockActor[i].Id == lastPost {
					noNew = true
				} else {
					lastPost = mockActor[i].Id
				}
			}
		}
		//	for i := 0; i < 82; i++ {
		//		fmt.Println("")
		//	}
		maxIndex = len(mockActor)
		index := 0
		var player libs.ActorS
		for index <= maxIndex && noNew == false {
			//reader := bufio.NewReader(os.Stdin)
			//input, _ = reader.ReadString('\n')
			fighting := false
			X := "1 1 1 1 1 1"
			Y := "2 3 4 5 6 7"
			player = getPC(client)
			drawMap(" ", client)
			inventory(player)

			physBattle(fighting, client)
			atac := PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel := libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "20 20 20 20 20 20"
			Y = "2 3 4 5 6 7"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "40 40 40 40 40 40"
			Y = "2 3 4 5 6 7"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "1 1 1 1 1 1"
			Y = "8 9 10 11 12 13"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "20 20 20 20 20 20"
			Y = "8 9 10 11 12 13"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "40 40 40 40 40 40"
			Y = "8 9 10 11 12 13"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "1 1 1 1 1 1"
			Y = "14 15 16 17 18 19"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "20 20 20 20 20 20"
			Y = "14 15 16 17 18 19"
			player = getPC(client)
			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			X = "40 40 40 40 40 40"
			Y = "14 15 16 17 18 19"
			player = getPC(client)

			inventory(player)
			physBattle(fighting, client)
			atac = PlaceCel(index, mockActor, X, Y, stdin, client)
			if atac == "atac" {
				fighting = true
			} else {
				fighting = false
			}

			drawMap(atac, client)
			atac = " "
			cel += libs.AssembleCel(mockActor[index], X, Y)
			fmt.Println(cel)
			index++
			if index >= maxIndex {
				break
			}
			fmt.Println(cel)
			if index >= maxIndex {
				break
			}

		}
		return true, borks
	}
	return false, borks
}
func LastPostSet(lastPost string) {
	newLP, err := os.Create("tests/lp")
	if err != nil {
		fmt.Println("Error creating file.")
	}
	newLP.Write([]byte(lastPost))
	newLP.Close()
	//fmt.Println("Last post set at" + lastPost)

}
func LastPostGet() string {
	actorBytes, err := ioutil.ReadFile("tests/lp")
	if err != nil {
		fmt.Println("Error, no actor file!")
	}

	LP := string(actorBytes)
	return LP
}
func UnmActors(currentActors string) []libs.ActorS {
	actorFile, err := os.Open(currentActors)
	defer actorFile.Close()
	defer os.Remove("tests/out.json")
	if err != nil {
		fmt.Println("Error, cannot find current actor file.")
	}
	actorReader, err := ioutil.ReadAll(actorFile)
	if err != nil {
		fmt.Println("Error reading actorfile.")
	}
	var mockup libs.ActorS
	var mockupRaw []json.RawMessage
	var messages []libs.ActorS
	err = json.Unmarshal(actorReader, &mockupRaw)
	if err != nil {
		fmt.Println("Error unmarshalling!")
		fmt.Println(err)
	}
	container := make(map[string]interface{})
	for c := 0; c < len(mockupRaw); c++ {

		err = json.Unmarshal(mockupRaw[c], &mockup)
		err = json.Unmarshal(mockupRaw[c], &container)
		if err != nil {
			fmt.Println("Error unmarshalling json.")
			fmt.Println(err)
		}
		k := make([]string, len(container))
		index := 0
		for i, _ := range container {
			k[index] = i

			if k[index] == "@context" {
				con := container["@context"]
				mockup.Context = con
			}
			index++
		}
		messages = append(messages, mockup)
	}
	//fmt.Println(k)
	//fmt.Println(string(actorReader))
	//fmt.Println(messages)
	return messages
}
func login(stdin chan string, client *http.Client) bool {
	instance := ""
	user := ""
	password = ""
	shownPass := ""
	for {
		for i := 0; i < 80; i++ {
			fmt.Println("")
		}
		fmt.Printf("\033[10;28H\033[0m")
		fmt.Printf("\033[11;28H \033[48;2;10;255;20m\033[38;2;10;10;255m         LOGIN         \033[0m")
		fmt.Printf("\033[12;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[13;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   \033[38;2;10;200;150mINSTANCE            \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[14;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   ________________    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[15;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[16;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   \033[38;2;10;200;150m                    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[17;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[18;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[19;28H \033[48;2;10;255;20m                       \033[0m")
		fmt.Printf("\033[14;32H" + instance + "\033[0m")
		input := <-stdin
		if input == "\n" {
			break
		}
		if input == "\u007F" {
			instance = instance[:len(instance)-1]
		} else {
			instance += input
		}
	}
	hostname = fmt.Sprint(instance)
	for {

		fmt.Printf("\033[10;28H\033[0m")
		fmt.Printf("\033[11;28H \033[48;2;10;255;20m\033[38;2;10;10;255m         LOGIN         \033[0m")
		fmt.Printf("\033[12;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[13;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   \033[38;2;10;200;150mUSER                \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[14;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   ________________    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[15;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[16;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   \033[38;2;10;200;150mPASSWORD            \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[17;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   ________________    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[18;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[19;28H \033[48;2;10;255;20m                       \033[0m")
		fmt.Printf("\033[14;32H" + user + "\033[0m")
		input := <-stdin
		if input == "\n" || input == "\t" {
			break
		}
		if input == "\u007F" {
			user = user[:len(user)-1]
		} else {
			user += input
		}
	}
	username = fmt.Sprint(user)
	for {

		fmt.Printf("\033[10;28H\033[0m")
		fmt.Printf("\033[11;28H \033[48;2;10;255;20m\033[38;2;10;10;255m         LOGIN         \033[0m")
		fmt.Printf("\033[12;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[13;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   \033[38;2;10;200;150mUSER                \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[14;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   ________________    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[15;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[16;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   \033[38;2;10;200;150mPASSWORD            \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[17;28H\033[48;2;10;255;20m \033[48;2;10;10;20m   ________________    \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[18;28H\033[48;2;10;255;20m \033[48;2;10;10;20m                       \033[48;2;10;255;20m \033[0m")
		fmt.Printf("\033[19;28H \033[48;2;10;255;20m                       \033[0m")
		fmt.Printf("\033[14;32H" + user + "\033[0m")
		fmt.Printf("\033[17;32H" + shownPass + "\033[0m")
		input := <-stdin
		if input == "\n" {
			break
		}
		if input == "\u007F" {
			password = password[:len(password)-1]
			shownPass = shownPass[:len(shownPass)-1]
		} else {
			password += input
			shownPass += "*"
		}
	}
	//	passF, err := os.Create("tests/pasf")

	//	defer os.Remove("tests/pasf")
	//	passF.Write([]byte(user + "\n" + password))
	//	message , err := ioutil.ReadFile("tests/pasf")

	//	if err != nil {
	//		fmt.Println("Error crafting login request")
	//	}

	//	messFile, err := os.Create("tests/spasf")

	//	if err != nil {
	//		fmt.Println("Error creating spasf")
	//	}
	//	defer os.Remove("tests/spasf")
	//	messFile.Write(message)

	sigBuffer, err := os.Create("tests/pasf")
	if err != nil {
		fmt.Println("Error creating signature.")
	}
	sig := fmt.Sprint(username + ":" + password)

	sigBuffer.Write([]byte(sig))
	sigBuffer.Close()
	signedContent, err := ioutil.ReadFile("tests/pasf")

	//	form := url.Values{}
	//	form.Add("en", string(sig))
	//req, err := http.NewRequest("GET", "https://"+hostname+":8080/examplelogin", nil)
	req, err := http.NewRequest("POST", "https://"+hostname+":8080/examplelogin", strings.NewReader(string(signedContent)))
	req.Header.Add("Signature", username+":"+password)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	//sigWriter := bufio.NewWriter(string(sig))
	//	req.Write(sigBuffer)

	//	reqUID, err := http.NewRequest("GET", "https://" + hostname + ":8080/getuid", strings.NewReader("BORKUIDPLS"))

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request")
		fmt.Println(err)
	}

	ok := resp.Header.Get("Authorization")
	//defer resp.Body.Close()
	if ok == "ok" {
		//	fmt.Println(ok)
		return true
	}
	return false
}
func initMap() {

}
func urgentMess(mess string) {
	space := "                                          "
	fmt.Printf("\033[36;61H\033[38;2;125;225;225m\033[48;2;125;225;225m  " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[37;61H\033[38;2;125;225;225m\033[48;2;125;225;225m\033[48;2;10;10;20m  " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[38;61H\033[38;2;125;225;225m\033[48;2;125;225;225m \033[48;2;10;10;20m " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[39;61H\033[38;2;125;225;225m\033[48;2;125;225;225m \033[48;2;10;10;20m " + mess + space[16:] + space[len(mess):] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[40;61H\033[38;2;125;225;225m\033[48;2;125;225;225m \033[48;2;10;10;20m " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[41;61H\033[38;2;125;225;225m\033[48;2;125;225;225m \033[48;2;10;10;20m " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[42;61H\033[38;2;125;225;225m\033[48;2;125;225;225m \033[48;2;10;10;20m " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[43;61H\033[38;2;125;225;225m\033[48;2;125;225;225m \033[48;2;10;10;20m " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")
	fmt.Printf("\033[44;61H\033[38;2;125;225;225m\033[48;2;125;225;225m  " + space + space[16:] + "\033[48;2;125;225;225m \033[0m")

}
func processColourMep(x int, y int, space string) {
	spaceSlice := space[x:y]
	//ProcessMep structure
	for i := range spaceSlice {
		switch string(spaceSlice[i]) {
		case "X":
			fmt.Printf("\033[38;2;0;250;0m")
			fmt.Printf(string(spaceSlice[i]))
			fmt.Printf("\033[0m")
		case "@":
			fmt.Printf("\033[38;2;100;100;235m")
			fmt.Printf(string(spaceSlice[i]))
			fmt.Printf("\033[0m")
		default:
			fmt.Printf(string(spaceSlice[i]))
			fmt.Printf("\033[0m")
		}
	}

}

func getRoom(desc string) {
	description := strings.Split(desc, ":")
	descIndex := len(description)
	index := 0
	space := "                                          "
	fmt.Printf("\033[36;1H\033[38;2;125;125;225m\033[48;2;125;125;225m\033[48;2;10;10;20m" + description[0] + space + space[:8] + "\033[48;2;125;125;225m    \033[0m")
	space = "======================"
	index++
	fmt.Printf("\033[37;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:17] + "\033[48;2;125;125;225m \033[0m")
	space = "                                          "
	if index < descIndex {
		fmt.Printf("\033[38;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[38;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space[:18] + space + "\033[48;2;125;125;225m \033[0m")

	}
	if index < descIndex {
		fmt.Printf("\033[39;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[39;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:18] + "\033[48;2;125;125;225m \033[0m")

	}
	if index < descIndex {
		fmt.Printf("\033[40;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[40;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:18] + "\033[48;2;125;125;225m \033[0m")

	}
	if index < descIndex {
		fmt.Printf("\033[41;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space + space + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[41;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:17] + "\033[48;2;125;125;225m \033[0m")

	}
	if index < descIndex {
		fmt.Printf("\033[42;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space + space + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[42;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:17] + "\033[48;2;125;125;225m \033[0m")

	}
	if index < descIndex {
		fmt.Printf("\033[43;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space + space + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[43;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:17] + "\033[48;2;125;125;225m \033[0m")

	}
	if index < descIndex {
		fmt.Printf("\033[44;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + description[index] + space + space + space[len(description[index]):] + "\033[48;2;125;125;225m \033[0m")
		index++
	} else {
		fmt.Printf("\033[44;1H\033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:17] + "\033[48;2;125;125;225m \033[0m")

	}
}
func physBattle(fighting bool, client *http.Client) {

	sigBuffer, err := os.Create("tests/pasf")
	if err != nil {
		fmt.Println("Error creating signature.")
	}
	sig := fmt.Sprint(username + ":" + password)

	sigBuffer.Write([]byte(sig))
	sigBuffer.Close()
	signedContent, err := ioutil.ReadFile("tests/pasf")
	req, err := http.NewRequest("POST", "https://"+hostname+":8080/getBattleTrace", strings.NewReader(string(signedContent)))
	req.Header.Add("Signature", username+":"+password)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	space := ""
	//sigWriter := bufio.NewWriter(string(sig))
	//	req.Write(sigBuffer)

	//	reqUID, err := http.NewRequest("GET", "https://" + hostname + ":8080/getuid", strings.NewReader("BORKUIDPLS"))

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request")
		fmt.Println(err)
	}

	ok := resp.Header.Get("Authorization")
	//defer resp.Body.Close()
	if ok == "ok" {
		//	fmt.Println(ok)

		hackTraceByte := resp.Header.Get("HackTrace")

		if err != nil {
			fmt.Println("Error reading map")
		}
		space = hackTraceByte
		if space == "OK" {
			space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
		} else {
			space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
		}
		//hackTraceByte, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Error reading map")
		}
		if fighting {
			//do something
			hacktracecl := "$>                   "

			fmt.Printf("\033[20;59H\033[38;2;125;125;225m\033[48;2;100;100;100m                            \033[48;2;125;125;225m \033[0m")
			overloadcl := "$>OVERLOAD           "
			overloadOverlay := ""
			for i := 0; i < len(overloadcl); i++ {
				overloadOverlay += fmt.Sprint("\033[48;2;150;150;0m ")
				space = fmt.Sprint("\033[48;2;250;0;0m  NO  \033[0m")
				fmt.Printf("\033[21;60H" + space + overloadOverlay)
				//	fmt.Printf("\033[21;66HOVERLOADING...")
				time.Sleep(100 * time.Millisecond)
			}
			var skill bool
			skill = true
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[21;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;100;100;100m " + space + overloadcl + "\033[48;2;100;100;100m \033[48;2;125;125;225m\033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[22;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m\033[48;2;100;100;100m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[23;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[24;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[25;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[26;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[27;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[28;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[29;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[30;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[31;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[32;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[33;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[34;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[35;59H\033[48;2;125;125;225m\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m                            \033[48;2;125;125;225m \033[0m")

			//begin map setup

			hacktracecl = "$>                   "

			space := "                    "
			fmt.Printf("\033[19;1H\033[48;2;125;125;225m\033[38;2;125;125;225m\033[48;2;10;10;20m" + hacktracecl + space + space[:17] + "\033[48;2;125;125;225m\033[0m")
			fmt.Printf("\033[20;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[21;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[22;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[23;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[24;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[25;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[26;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[27;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[28;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[29;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[30;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[31;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[32;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[33;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[34;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[35;1H\033[48;2;125;125;225m\033[48;2;125;125;225m \033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")

		} else {
			hacktracecl := "$>                   "

			fmt.Printf("\033[20;59H\033[38;2;125;125;225m\033[48;2;100;100;100m                            \033[48;2;125;125;225m \033[0m")
			overloadcl := "$>OVERLOAD           "

			var skill bool
			skill = true
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[21;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;100;100;100m " + space + overloadcl + "\033[48;2;100;100;100m \033[48;2;125;125;225m\033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[22;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m\033[48;2;100;100;100m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[23;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[24;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[25;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[26;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[27;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[28;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[29;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[30;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[31;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[32;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[33;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[34;59H\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m " + space + hacktracecl + "\033[48;2;125;125;225m \033[0m")
			skill = false
			if skill == true {
				space = fmt.Sprint("\033[48;2;0;250;0m  OK  \033[0m")
			} else {
				space = fmt.Sprint("\033[48;2;100;100;100m NULL \033[0m")
			}
			fmt.Printf("\033[35;59H\033[48;2;125;125;225m\033[38;2;125;125;225m\033[48;2;10;10;20m\033[48;2;0;100;100m                            \033[48;2;125;125;225m \033[0m")
			//begin map setup

			hacktracecl = "$>                   "
			space := "                    "
			fmt.Printf("\033[19;1H\033[48;2;125;125;225m\033[38;2;125;125;225m\033[48;2;10;10;20m" + hacktracecl + space + space[:17] + "\033[48;2;125;125;225m\033[0m")
			fmt.Printf("\033[20;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[21;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[22;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[23;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[24;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[25;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[26;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[27;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[28;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[29;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[30;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[31;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[32;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[33;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[34;1H\033[38;2;125;125;225m\033[48;2;125;125;225m \033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
			fmt.Printf("\033[35;1H\033[48;2;125;125;225m\033[48;2;125;125;225m \033[38;2;125;125;225m\033[48;2;10;10;20m" + space + space + space[:15] + "\033[48;2;125;125;225m \033[0m")
		}
	}
}
func drawMap(direction string, client *http.Client) {
	sigBuffer, err := os.Create("tests/pasf")
	if err != nil {
		fmt.Println("Error creating signature.")
	}
	sig := fmt.Sprint(username + ":" + password)

	sigBuffer.Write([]byte(sig))
	sigBuffer.Close()
	signedContent, err := ioutil.ReadFile("tests/pasf")
	req, err := http.NewRequest("POST", "https://"+hostname+":8080/getMap", strings.NewReader(string(signedContent)))
	req.Header.Add("Signature", username+":"+password)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Direction", direction)
	//sigWriter := bufio.NewWriter(string(sig))
	//	req.Write(sigBuffer)

	//	reqUID, err := http.NewRequest("GET", "https://" + hostname + ":8080/getuid", strings.NewReader("BORKUIDPLS"))

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request")
		fmt.Println(err)
	}

	ok := resp.Header.Get("Authorization")
	//defer resp.Body.Close()
	if ok == "ok" {
		//	fmt.Println(ok)
		roomDesc := resp.Header.Get("Room Desc")
		getRoom(roomDesc)
		roomActions := resp.Header.Get("Room Actions")
		urgentMess(roomActions)
		spaceByte, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Error reading map")
		}
		space := string(spaceByte)
		num := 15
		//begin map setup
		fmt.Printf("\033[1;62H\033[0m")
		fmt.Printf("\033[2;62H \033[48;2;100;100;235m" + "\033[38;2;10;10;20mMap              " + "\033[0m")
		fmt.Printf("\033[3;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[4;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[5;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[6;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[7;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[8;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[9;62H\033[48;2;100;100;235m \033[38;2;100;100;235m\033[48;2;10;10;20m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[10;62H\033[48;2;100;100;235m\033[38;2;100;100;235m \033[48;2;10;10;20m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[11;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[12;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[13;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[14;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[15;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[16;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[17;62H\033[48;2;100;100;235m \033[48;2;10;10;20m\033[38;2;100;100;235m")
		processColourMep(num, num+17, space)
		num += 17
		fmt.Printf("\033[48;2;100;100;235m \033[0m")
		fmt.Printf("\033[18;62H \033[48;2;100;100;235m" + "                 " + "\033[0m")
		fmt.Printf("\033[19;62H\033[0m")
	}

}
func inventory(actor libs.ActorS) {
	space := "                     "
	fmt.Printf("\033[1;83H\033[0m")
	fmt.Printf("\033[2;83H \033[48;2;100;100;235m\033[38;2;10;10;20mInventory" + space[:len(space)-9] + "\033[0m")

	if len(actor.Inventory) > 0 {
		item := actor.Inventory[0]
		space = item + space[len(item):]
		fmt.Printf("\033[3;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
		space = "                     "

	} else {
		fmt.Printf("\033[3;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	}
	fmt.Printf("\033[4;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[5;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[6;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[7;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[8;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[9;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[10;83H\033[48;2;100;100;235m\033[38;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[11;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[12;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[13;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[14;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[15;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[16;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[17;83H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[18;83H \033[48;2;100;100;235m" + space + "\033[0m")
	fmt.Printf("\033[19;83H\033[0m")

	space = "                                   "
	fmt.Printf("\033[1;103H\033[0m")
	fmt.Printf("\033[2;103H \033[48;2;100;100;235m\033[38;2;10;10;20mEquipment" + space[:len(space)-9] + "\033[0m")
	if len(actor.Inventory) > 0 {
		item := actor.Equipment["body"]
		space = item + space[len(item):]
		fmt.Printf("\033[3;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
		space = "                                   "

	} else {
		fmt.Printf("\033[3;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")

	}

	fmt.Printf("\033[4;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[5;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[6;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[7;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[8;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[9;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[10;103H\033[48;2;100;100;235m\033[38;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[11;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[12;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[13;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[14;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[15;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[16;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[17;103H\033[48;2;100;100;235m \033[48;2;10;10;20m" + space + "\033[48;2;100;100;235m \033[0m")
	fmt.Printf("\033[18;103H \033[48;2;100;100;235m" + space + "\033[0m")
	fmt.Printf("\033[19;103H\033[0m")

}
func main() {
	cer, err := tls.LoadX509KeyPair("/home/twotonne/go/src/gitlab.com/owlo/server.rsa.crt", "/home/twotonne/go/src/gitlab.com/owlo/server.rsa.key")
	if err != nil {
		log.Println(err)
	}

	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	tr := &http.Transport{
		TLSClientConfig: config,
	}
	client := &http.Client{Transport: tr}
	//	fmt.Println("\033[38;2;255;0;0mYou have found the DEV CONSOLE!\nWelcome! Things will break, often. Some things will do things that you don't expect and change without notice. Here are some buttons you can press to do things, enjoy!\033[0m")
	//
	//	fmt.Println("Press \033[38;2;255;0;0m1\033[0m to get the local objects from \033[48;2;235;20;20mlocalhost\033[0m")
	//	fmt.Println("Press \033[38;2;255;0;0m2\033[0m to get the local actors from \033[48;2;235;20;20mlocalhost\033[0m")
	//	fmt.Println("Press \033[38;2;255;0;0m3\033[0m to get a local test post from \033[48;2;235;20;20mlocalhost\033[0m")
	//	REMOVED
	for i := 0; i < 32; i++ {
		fmt.Println("")
	}
	//this is a placeholder and does nothing right now

	stdin := make(chan string, 1)
	kill := make(chan bool, 1)
	go HandleInput(stdin, kill)
	ok := login(stdin, client)
	if ok != true {
		fmt.Println("WRONG PASSWORD")
		os.Exit(1)
	}
	lp := LastPostGet()
	for {
		//lp = LastPostGet()
		//fmt.Println(lp)
		done, lp := GetCels(stdin, lp, client)
		//LastPostSet(lp)
		if done {
			fmt.Printf("\033[19;3HDONE GRABBING CELS")
			fmt.Printf("\033[19;3HLAST POST" + lp)
		}
		fmt.Printf("\033[19;3HNO NEW BORKS, AWAITING COMMAND")
		//command := <-stdin
		//command = ParseInput(command, stdin, client)
		//fmt.Println(command)
	}

}
